package com.example.bottom_nevigation

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout


class MainActivity2 : AppCompatActivity() {

    private lateinit var pager: ViewPager // creating object of ViewPager
    private lateinit var tab: TabLayout // creating object of TabLayout
    private lateinit var bar: Toolbar // creating object of ToolBar

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        pager = findViewById(R.id.tab_viewpager)
        tab = findViewById(R.id.tab_tablayout)

//        bar = findViewById(R.id.toolbar)
//        setSupportActionBar(bar)


        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(ChartFragment(), "Chat")
        adapter.addFragment(StatusFragment(), "Status")
        adapter.addFragment(CallFragment(), "Call")

        pager.adapter = adapter

        // bind the viewPager with the TabLayout.
        tab.setupWithViewPager(pager)
    }

    private fun setSupportActionBar(bar: Toolbar?) {

    }


}




